# Воробьев Андрей, вариант 9


def input_line():
    """
    Функция для ввода строки
    :return: введенное выражение
    """
    old_line = input('Введите строку: ')
    return old_line


def check_str_one_stars(line):
    """
    Функция проверяет, чтобы две звездочки стояли в конце и в начале выражения,
    в противном случае возвращает False.
    :param line: строка
    :return: False or True
    """
    if type(line) != str:
        print(type(line))
        print('Вы ввели не строку')
        return False
    if line.count('*') != 2:
        print('Количество звездочек должно быть 2')
        return False
    if line.find('*') != 0 or not line.endswith('*'):
        print('Звездочки должны быть в начале и в конце')
        return False
    if line.find('*') == 0 and line.endswith('*'):
        return True


def one_stars(str_r):
    """
    Функция заменяет выражение, обрамленное в одинарные звездочки, на <em> и </em>
    для каждой звездочки соответственно.
    :param str_r: строка
    :return: печать преобразованной строки
    """
    new_str = str_r.replace('*', '<em>', 1).replace('*', '</em>', 1)
    print(f'Результат: {new_str}')
    print()


def replace_one_stars():
    """
    Функция замены одинарных звездочек в начале и в конце предложения
    на <em> и </em> соответственно.
    :return: преобразованная строка
    """
    while True:
        srk = input_line()
        if check_str_one_stars(srk):
            one_stars(srk)
            break


def check_str_two_stars(line):
    """
    Функция проверяет, чтобы четыре звездочки стояли по две в конце и в начале выражения,
    в противном случае возвращает False.
    :param line: строка
    :return: False or True
    """
    if type(line) != str:
        print(type(line))
        print('Вы ввели не строку')
        return False
    if line.count('*') != 4:
        print('Количество звездочек должно быть 4')
        return False
    if line.find('**') != 0 or not line.endswith('**'):
        print('Звездочки должны быть в начале и в конце попарно')
        return False
    if line.find('**') == 0 and line.endswith('**'):
        return True


def two_stars(str_r):
    """
    Функция заменяет выражение, обрамленное в двойные звездочки, на <strong> и </strong>
    для каждой пары звездочек соответственно.
    :param str_r: строка
    :return: печать преобразованной строки
    """
    new_str = str_r.replace('**', '<strong>', 1).replace('**', '</strong>', 1)
    print(f'Результат: {new_str}')
    print()


def replace_two_stars():
    """
    Функция замены двойных звездочек в начале и в конце предложения
    на <strong> и </strong> соответственно.
    :return: преобразованная строка
    """
    while True:
        srk = input_line()
        if check_str_two_stars(srk):
            two_stars(srk)
            break


def main():
    while True:
        choice = input("Выберите действие:\n"
                       "a) заменить одинарные звездочки;\n"
                       "b) заменить двойные звездочки;\n"
                       "с) выход из программы.\n")
        if choice == 'a':
            replace_one_stars()
        elif choice == 'b':
            replace_two_stars()
        elif choice == 'c':
            print('Программа завершена')
            break
        else:
            print('Вы ввели неправильное значение. Попробуйте снова.')
            continue


main()
# добавлена актуальная версия
